import { useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';

export default function AdminView(props){
	// console.log(props)
	//destructure the productsProp and the fetchData function from Products.js
	const { productsProp, fetchData } = props;

	const [productsArr, setProductsArr] = useState([])
	const [productId, setProductId] = useState("")
	const [productName, setProductName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0)
	const [addProduct, setAddProduct] = useState([])
	const [showAddProduct, setShowAddProduct] = useState(false)
	//states for handling modal visibility
	const [showEdit, setShowEdit] = useState(false)

	const token = localStorage.getItem("token")

	//Functions to handle opening and closing modals
	const openEdit = (productId) => {
		// console.log(productId)
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
			.then(res => res.json())
			.then(data => {
				// console.log(data)
				setProductId(data._id)
				setProductName(data.productName)
				setDescription(data.description)
				setPrice(data.price)
			})
		setShowEdit(true)
	}

	const openAddProduct = () => {
		setShowAddProduct(true);
	}

	const closeEdit = () => {
		setProductId("")
		setProductName("")
		setDescription("")
		setPrice(0)
		setShowEdit(false)
		setShowAddProduct(false)
	}


	const editProduct = (e) => {
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: productName,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				alert("Product successfully updated")
				// Close the modal and set all states back to default values
				closeEdit()
				fetchData()
				// We call fetchData here to update the data we receive from the database
				// Calling fetchData updates our productsProp, which the useEffect below is monitoring
				// Since the productProp udpates, the useEffect runs again, which re-renders our table with the updated data
			}else {
				alert("Something went wrong")
			}
		})
	}

	const archiveToggle = (productId, isActive) => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				let bool

				isActive ? bool ="disabled" : bool = "enabled"

				alert(`Product successfully ${bool}`)

				fetchData()
			}else {
				alert(`Something went wrong`)
			}
		})

	}

	const addNewProduct = (e) => {
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/products`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				alert(`Product successfully added`)
				closeEdit()
				fetchData()
			}else {
				alert(`Something went wrong`)
			}
		})

	}

	// const closeShowAddProduct = () => {
	// 	setAddProduct("")
	// 	setShowAddProduct(false)
	// }

	useEffect(() => {
		//map through the productsProp to generate table contents
		const products = productsProp.map(product => {
			return(
				<tr key={product._id}>
					<td>{product.productName}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>
							{/*Dynamically render product availability*/}
							{product.isActive
								? <span>Available</span>
								: <span>Unavailable</span>
							}
					</td>
					<td>
						<Button variant="primary" size="sm" onClick={() => openEdit(product._id)}>Update</Button>
						{product.isActive
							//dynamically render which button show depending on product availability
							? <Button variant="danger" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Disable</Button>
							: <Button variant="success" size="sm" onClick={() => archiveToggle(product._id, product.isActive)}>Enable</Button>
						}
					</td>
				</tr>
			)
		})

		//set the ProductsArr state with the results of our mapping so that it can be used in the return statement
		setProductsArr(products)

	}, [productsProp])
	
	return(
		<>
			<h1 className="text-center">ADMIN DASHBOARD</h1>
			<Button className="mb-1 me-1" variant="primary" size="lg" onClick={() => openAddProduct()}>Add New Product</Button>
			<Button className="mb-1 ms-1" variant="secondary" size="lg">View User Orders</Button>

			{/*Product info table*/}
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{/*Mapped table contents dynamically generated from the productsProp*/}
					{productsArr}
				</tbody>
			</Table>

			{/*Edit Product Modal*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Update Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={productName}
								onChange={e => setProductName(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={description}
								onChange={e => setDescription(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>


			{/*Add Product Modal*/}
			<Modal show={showAddProduct} onHide={closeEdit}>
				<Form onSubmit={e => addNewProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add New Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control
								value={productName}
								onChange={e => setProductName(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control
								value={description}
								onChange={e => setDescription(e.target.value)}
								type="text"
								required
							/>
						</Form.Group>

						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control
								value={price}
								onChange={e => setPrice(e.target.value)}
								type="number"
								required
							/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

		</>
	)
}
