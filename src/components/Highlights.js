import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
    return(
        <Row className="my-3">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h3>Air Jordan 1 Retro High OG</h3>
                        </Card.Title>
                        <Card.Text>
                            {/*<img src="https://www.nike.com/ph/t/air-jordan-1-retro-high-og-shoes-G8hcQx/555088-037" className="img-fluid"/> */}
                           Familiar but always fresh, the iconic Air Jordan 1 is remastered for today's sneakerhead culture. This Retro High OG version goes all in with premium leather, comfortable cushioning and classic design details.
                        </Card.Text>
                    </Card.Body>
                </Card>
                </Col>

                <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h3>Air Jordan 5 Retro</h3>
                        </Card.Title>
                        <Card.Text>
                           Stand out from the crowd. The Air Jordan 5 Retro brings back a coveted 2006 release done up in grey-reflectivity design and, of course, bright green accents.
                        </Card.Text>
                    </Card.Body>
                </Card>
                </Col>

                <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Air Jordan 13 Retro</h2>
                        </Card.Title>
                        <Card.Text>
                           Michael Jordan earned the nickname Black Cat by his superior athletic ability and prowess at both ends of the court.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            
        </Row>
    )
}