import {Row, Col, Card, CardBody} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import { Link, useNavigate } from 'react-router-dom';

// Destructuring is done in the parameter to retrieve the courseProp
export default function ProductCard({productProp}){

    // console.log(props.courseProp);
    // console.log(courseProp);

    const {_id, productName, description, price} = productProp;

    const navigate = useNavigate();


    const onClick = () => {
        
    }

    return(
        <Card className="mb-2" onClick={() => { navigate(`/products/${_id}`) }} style={{ cursor: "pointer" }}>
            <Card.Body >
                <Card.Title>{productName}</Card.Title>
                <Card.Subtitle className="mb-0">Description:</Card.Subtitle>
                <Card.Text className="mt-0">{description}</Card.Text>
                <Card.Subtitle className="mb-0">Price:</Card.Subtitle>
                <Card.Text className="mt-0">PhP {price}</Card.Text>
            </Card.Body>
        </Card>

    );
}

