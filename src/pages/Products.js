import { useEffect, useState, useContext } from 'react';
// import courseData from '../data/courseData';
import ProductCard from '../components/ProductCard';
import AdminView from '../components/AdminView';
import UserContext from '../UserContext'
// import ProductContext from '../ProductContext'

export default function Products() {

    
    const [productsData, setProductsData] = useState([])
    const { user } = useContext(UserContext);
    

    const fetchData = () => {
        // console.log(`${process.env.REACT_APP_API_URL}`)
        fetch(`${process.env.REACT_APP_API_URL}/products/all`)
        .then(res => res.json())
        .then(data => {
           
            setProductsData(data);
            // console.log(productsData)
        })
    }

    console.log(user.isAdmin);
    useEffect(() => {
        fetchData();
    }, [])

    const products = productsData.map(products => {
        if(products.isActive){
            return (
                <ProductCard productProp={products} key={products._id}/>
            )
        }else{
            return null
        }
    })

    return(
        (user.isAdmin) ?
        <AdminView productsProp={productsData} fetchData={fetchData}/>
        :
        <>
            <h1>Products</h1>
            {products}
        </>
    )
}