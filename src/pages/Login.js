import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext'
import { Navigate, useNavigate } from 'react-router-dom';

export default function Login(){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	const{user, setUser} = useContext(UserContext);
	const navigate = useNavigate();

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password])

	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// set our user state to include the user's id and isAdmin values
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	function authenticateUser(e){
		e.preventDefault() //prevent default form behavior, so that the form does not submit


		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(typeof data.access !== "undefined"){
				localStorage.setItem("token", data.access)
				// call the retrieveUserDetails function and pass the JWT to it
				retrieveUserDetails(data.access)

				alert("Thank you for logging in!");
				navigate('/products')
				// console.log(data);
			}else{
				alert("Email or Password does not match!");
				setEmail("");
				setPassword("");
			}
		})


		// alert('Thank you for logging in');

	}

	return(
			(user.id === null) ?
			<Form onSubmit={e => authenticateUser(e)}>
				<Form.Group controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email"
						value={email}
						onChange={e => setEmail(e.target.value)}
						required
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group controlId="password">
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter password"
						value={password}
						onChange={e => setPassword(e.target.value)}
						required
					/>
				</Form.Group>

				{isActive ?
					<Button className="mt-3" variant="primary" type="submit" id="submitBtn">
					Submit
					</Button>
					:
					<Button className="mt-3" variant="primary" id="submitBtn" disabled>
					Submit
					</Button>				
				}
			</Form>
			
			:

			<Navigate to="/"/>
	)
}