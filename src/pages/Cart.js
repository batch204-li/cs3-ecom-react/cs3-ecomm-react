import { useEffect, useState, useContext } from 'react';
import { Button, Stack } from 'react-bootstrap'
// import courseData from '../data/courseData';
import CartCard from '../components/CartCard';
import EmptyCart from '../components/EmptyCart';
import AdminView from '../components/AdminView';
import UserContext from '../UserContext'
import { useNavigate } from 'react-router-dom'


export default function Cart() {

    const [cartsData, setCartsData] = useState([])
    const [subTotal, setSubTotal] = useState(0)

    const [count, setCount] = useState([])
    const { user } = useContext(UserContext);

    const token = localStorage.getItem("token")
    const navigate = useNavigate();


    const fetchUserData = () => {
        // console.log(`${process.env.REACT_APP_API_URL}`)
        // e.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/users/cart`, {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            // const index = data.
            // console.log(data);
            if(data.isAdmin === false){
                setCartsData(data.cart);
                // console.log(data.cart)
            }else {
                navigate("/products")
            }
        })
    }

    // Order checkout
    const orderCheckout = () => {
        // totalAmount();
        fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify({
                products: cartsData,
                totalAmount: subTotal
            })
        })
        .then(res => res.json())
        .then(data => {
            if(true) {
                console.log(data)
                removeAllFromCart();
                alert(`Thank you for your purchase!`)
                fetchUserData()
            }else {
                alert(`Something went wrong`)
            }
        })
    }

    // Remove all from cart
    const removeAllFromCart = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/removeAll`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            }
        })
    }

    // Total amount in cart
    const totalAmount = () => {
        // console.log(cartsData)
        let sub = 0
        for(let i = 0; i < cartsData.length; i++) {
            sub = sub + (cartsData[i].price * cartsData[i].quantity)
        }
            // console.log(cartsData[0].price *  cartsData[0].quantity)
        // console.log(sub)
        setSubTotal(sub)
    }
 
    useEffect(() => {
        totalAmount();
        fetchUserData();
        // console.log(c)
    }, [cartsData])

    const carts = cartsData.map(carts => {
        if(carts !== null){
            // console.log(cartsData)
            // console.log(carts._id)   
            return (
                <CartCard cartProp={carts} key={carts._id} cartsData={cartsData} fetchUserData={fetchUserData}/>
            )
        }else{
            return null
        }
    })

    

    return(
        (cartsData[0] !== undefined) ?
        <>
            <Stack direction="horizontal" gap={2}>
                <h1 className="">Shopping Cart</h1>
                <div className="border ms-auto">
                    <Button size="lg" onClick={orderCheckout}>Check Out</Button>
                    {/*<Button onClick={totalAmount}>TOTAL</Button>*/}
                </div>           
            </Stack> 
            {carts}
            {/*{console.log(cartsData[0])}*/}
        </>
        :
        <>
            <h1 className="text-center">Shopping Cart</h1>
            <EmptyCart />
        </>
    )
}
