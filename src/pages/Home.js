import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

// Added something

export default function Home(){

	const data = {
		title: "AIR JORDAN",
		content: <h3>"Your First Step to Greatness!"</h3>,
		destination: "/products",
		label: "Browse Products!"
	}
	return(
		<>
			<Banner dataProp={data} />
			<Highlights />
		</>
	)
}