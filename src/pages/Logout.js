import { useEffect, useContext } from 'react';
// import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext'


export default function Logout(){

	const{setUser} = useContext(UserContext);

	localStorage.clear();

	useEffect(() => {
		setUser({
			id: null,
			isAdmin: null
		})
	})

	return(
			<Navigate to="/login"/>
		)
}